angular.module('starter')
.controller('AdminCtrl', function($log, $scope, $location, $ionicLoading, $localStorage, 
	$ionicPopup, $window, $cordovaGeolocation, $ionicModal, Estimates, $ionicPlatform, $timeout){
	
	vm = this;

    vm.favorites = $localStorage.favorites? $localStorage.favorites : [];
    vm.history = $localStorage.history? $localStorage.history : [];
    vm.origin = 0;
    vm.searchFrom = '';
    vm.adminShow = true;
    vm.house = ($localStorage.house && $localStorage.house.trim != '') ? $localStorage.house:'';
    vm.work = ($localStorage.work && $localStorage.house.work != '') ? $localStorage.work:'';

    $ionicModal.fromTemplateUrl('search_modal.html', {

        scope: $scope,

        animation: 'slide-in-up'

    }).then(function(modal) {

        $scope.modal = modal;

    });

	vm.back = function(){
        window.history.go(-1); 
    }

    vm.home = function(){
        $window.location.href = '#/app/principal';
    }

    vm.addFavorite = function(favorite){

        vm.favorites.push(favorite);

        $localStorage.favorites = vm.favorites;

    }

    vm.removeFavority = function(favority){

        var message = $ionicPopup.confirm({

            title: "Remover Favorito",

            template: "Tem certeza que deseja remover este endereço da lista de favoritos?"

        });

        message.then(function(res) {
            
            if(res){

                vm.favorites.splice(vm.favorites.indexOf(favority),1)

            }

        });

    }

    vm.openModal = function(origin) {

        vm.origin = origin;

        vm.searchFrom = '';
        
        vm.adminShow = false;

        $scope.modal.show();

    };

    $ionicPlatform.onHardwareBackButton(function() {
        $scope.closeModal();
    });

    $scope.closeModal = function() {

        $timeout(function(){ 
            vm.adminShow = true;
        }, 300);
        
        $scope.modal.hide();

    };

    vm.selectAddress = function(place){

        $ionicLoading.show({
            content: 'Carregando...',
            animation: 'fade-in',
            noBackdrop: false
        });

        $scope.closeModal();

        var location = place;

        if(vm.origin == 3){

            vm.adminShow = true;

            vm.house = location;

            vm.houseLocation = location;

            $localStorage.house = location;

        }else if(vm.origin == 4){

            vm.adminShow = true;

            vm.work = location;

            vm.workLocation = location;

            $localStorage.work = location;

        }
        
        $ionicLoading.hide();

    }

    var count = 0;

    var arraySearch = [];

    vm.getPlaces = function(inputValue){

        var contActual = (JSON.parse(JSON.stringify(count++)));

        arraySearch[contActual] = inputValue;

        $timeout(function(){ 

            if(contActual == (arraySearch.length - 1)){

                arraySearch = [];

                count = 0;

                if(inputValue.length > 4){

                    if(vm.latitude == 0){

                        $cordovaGeolocation.getCurrentPosition(options).then(function(position){

                            Estimates.nearbysearch(inputValue, position.coords.latitude, position.coords.longitude).then(function(response){


                                if(response && response.predictions && response.predictions.length > 0){

                                    vm.resultPlaces = response.predictions;

                                }else{

                                    vm.resultPlaces = [];

                                }

                            }, function(error){

                                Estimates.places(inputValue).then(function(response){

                                    if(response && response.predictions && response.predictions.length > 0){

                                        vm.resultPlaces = response.predictions;

                                    }else{

                                        vm.resultPlaces = [];

                                    }

                                }, function(error){

                                    vm.resultPlaces = [];

                                });

                            });

                        }, function(error){

                            Estimates.places(inputValue).then(function(response){

                                if(response && response.predictions && response.predictions.length > 0){

                                    vm.resultPlaces = response.predictions;

                                }else{

                                    vm.resultPlaces = [];

                                }

                            }, function(error){

                                vm.resultPlaces = [];

                            });

                        });

                    }else{

                         Estimates.nearbysearch(inputValue, vm.latitude, vm.longitude).then(function(response){

                            if(response && response.predictions && response.predictions.length > 0){

                                vm.resultPlaces = response.predictions;

                            }else{

                                vm.resultPlaces = [];

                            }

                        }, function(error){

                            Estimates.places(inputValue).then(function(response){

                                if(response && response.predictions && response.predictions.length > 0){

                                    vm.resultPlaces = response.predictions;

                                }else{

                                    vm.resultPlaces = [];

                                }

                            }, function(error){

                                vm.resultPlaces = [];

                            });

                        });

                    }

                }else{

                    vm.resultPlaces = [];

                }
            }

        }, 600);

    }

    vm.selectResultPlaces = function(place){

        $ionicLoading.show({
            content: 'Carregando...',
            animation: 'fade-in',
            noBackdrop: false
        });

        $scope.closeModal();

        Estimates.detailsPlace(place.place_id).then(function(response){

            if(response && response.result){

                var result = response.result;

                var district = "";
                var city = "";
                var state = "";
                var zipcode = "";
                var number = "";

                result.address_components.forEach(function(e){

                    if(e.types && e.types.length > 0){

                        e.types.forEach(function(t){

                            if("sublocality_level_1" == t){
                                district = e.short_name;
                                return;
                            }else if("administrative_area_level_2" == t){
                                city = e.short_name;
                                return;
                            }else if("administrative_area_level_1" == t){
                                state = e.short_name;
                                return;
                            }else if("postal_code" == t){
                                zipcode = e.short_name;
                                return;
                            }else if("street_number" == t){
                                number = e.short_name;
                                return;
                            }

                        });

                    }

                });


                if(number == ""){

                    var numbers = place.description.split(",")

                    if(numbers.length > 0){

                        var numberSplit = (numbers[1].match(/\d+/));

                        if(numberSplit && numberSplit.length > 0){

                            number = numberSplit[0];

                        }

                    }else{

                        number = "";

                    }

                }

                if(place.structured_formatting){
                    if(place.structured_formatting.main_text){
                        place.structured_formatting.main_text = place.structured_formatting.main_text.replace(";", ",");
                    }
                    if(place.structured_formatting.secondary_text){
                        place.structured_formatting.secondary_text = place.structured_formatting.secondary_text.replace(";", ",");
                    }
                }

                var location = {
                    location: {
                        lat: result.geometry.location.lat, 
                        lng: result.geometry.location.lng,
                        state: state,
                        city: city,
                        district: district,
                        address: place.description,
                        zipcode: zipcode,
                        number: number,
                        structured_formatting: place.structured_formatting
                    },  
                    description: result.formatted_address
                };

                if(vm.origin == 3){

                    vm.adminShow = true;

                    vm.house = location.location;

                    vm.houseLocation = location.location;

                    $localStorage.house = location.location;

                }else if(vm.origin == 4){

                    vm.adminShow = true;

                    vm.work = location.location;

                    vm.workLocation = location.location;

                    $localStorage.work = location.location;

                }

            }

            $ionicLoading.hide();

        }, function(error){

            $ionicLoading.hide();

            $log.error(error);

            /* $ionicPopup.alert({

                title: 'Erro nÃÂ£o esperado',

                template: 'Obtivemos um erro ao acessar nossos serviÃÂ§os. Verifique sua conexÃÂ£o com a internet e tente novamente.'

            }); */

        });

    }

    vm.cleanSearch = function(){
        vm.searchFrom = "";
    }

});
