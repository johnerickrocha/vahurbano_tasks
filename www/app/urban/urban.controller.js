angular.module('starter')
.controller('UrbanCtrl', function($log, $scope, $location, $ionicModal, $ionicLoading, $timeout, $localStorage, 
    $ionicPopup, $window, $cordovaGeolocation, Estimates, $cordovaNetwork, $http, $cordovaAppAvailability, 
    $ionicPlatform, $cordovaInAppBrowser){


    vm = this;

    vm.from = '';
    vm.to = '';
    vm.origin = 0;
    vm.placeholderOrigin = "Adicionar origem";
    vm.placeholderTo = "Adicionar destino";
    vm.searchFrom = '';
    vm.resultPlaces = [];
    vm.placeSelected = {};
    vm.fromLocation = {};
    vm.toLocation = {};
    vm.myPosition = {};
    vm.fromFavoriteIcon = 'ion-android-star-outline';
    vm.toFavoriteIcon = 'ion-android-star-outline';
    vm.favorites = $localStorage.favorites? $localStorage.favorites : [];;
    vm.history = $localStorage.history? $localStorage.history : [];
    $localStorage.history = $localStorage.history? $localStorage.history : [];
    vm.listDirections = [];
    vm.house = ($localStorage.house && $localStorage.house.trim != '') ? $localStorage.house:'';
    vm.work = ($localStorage.work && $localStorage.house.work != '') ? $localStorage.work:'';

    vm.easy = false;
    vm.taxis99 = false;
    vm.uber = false;
    vm.cabify = false;
    vm.yetgo = false;
    vm.isResult = false;

    vm.searchShow = true;
    vm.resultShow = false;
    vm.adminShow = false;

    vm.iconHeader = "img/icon-car2.png";

    $ionicPlatform.onHardwareBackButton(function() {
        $scope.closeModal();
    });

    vm.showMessageStart = function(){

        var lastId = Estimates.selectMaxPopup();

        if(lastId < 0){
            lastId = 0
        }

        Estimates.getLastMessage(lastId).then(function(result){

            if(result && result.id > 0){

                Estimates.insertPopup(result);

                var confirmPopup =  $ionicPopup.alert({

                    title: 'Mensagem',

                    template: result.message

                });

                confirmPopup.then(function(res) {

                    if(result.link && result.link.length > 0){

                        window.open(result.link,'_system');

                    }else{

                        $window.location.href = '#/app/menu';

                    }

                });
            }

        }, function(error){


        });
    }

    vm.showMessageStart();

    document.addEventListener("deviceready", function () {



        $cordovaAppAvailability.check('uber://').then(function() {
            vm.uber = true;
        }, function () {
            $cordovaAppAvailability.check('com.ubercab').then(function() {
                vm.uber = true;
            }, function () {
                vm.uber = false;
            });
        });

        $cordovaAppAvailability.check('cabify://').then(function() {
            vm.cabify = true;
        }, function () {
            $cordovaAppAvailability.check('com.cabify.rider').then(function() {
                vm.cabify = true;
            }, function () {
                vm.cabify = false;
            });
        });

        $cordovaAppAvailability.check('taxis99://').then(function() {
            vm.taxis99 = true;
        }, function () {
            $cordovaAppAvailability.check('com.taxis99').then(function() {
                vm.taxis99 = true;
            }, function () {
                vm.taxis99 = false;
            });
        });

        $cordovaAppAvailability.check('easytaxi://').then(function() {
            vm.easy = true;
        }, function () {
            $cordovaAppAvailability.check('br.com.easytaxi').then(function() {
                vm.easy = true;
            }, function () {
                vm.easy = false;
            });
        });

        $cordovaAppAvailability.check('yetgo://').then(function() {
            vm.yetgo = true;
        }, function () {
            $cordovaAppAvailability.check('com.br.yetgo').then(function() {
                vm.yetgo = true;
            }, function () {
                vm.yetgo = false;
            });
        });

    }, false);

    $ionicLoading.show({
        content: 'Carregando...',
        animation: 'fade-in',
        noBackdrop: false
    });

    var iconStart = {
        url: "img/marker_google_start.png", // url
        scaledSize: new google.maps.Size(50, 50), // scaled size
        origin: new google.maps.Point(0,0), // origin
        anchor: new google.maps.Point(15, 50) // anchor
    };

    var iconEnd = {
        url: "img/marker_google_end.png", // url
        scaledSize: new google.maps.Size(50, 50), // scaled size
        origin: new google.maps.Point(0,0), // origin
        anchor: new google.maps.Point(15, 50) // anchor
    };

    var iconHouse = {
        url: "img/marker_house.png", // url
        scaledSize: new google.maps.Size(50, 50), // scaled size
        origin: new google.maps.Point(0,0), // origin
        anchor: new google.maps.Point(15, 50) // anchor
    };

    var iconJob = {
        url: "img/marker_job.png", // url
        scaledSize: new google.maps.Size(50, 50), // scaled size
        origin: new google.maps.Point(0,0), // origin
        anchor: new google.maps.Point(15, 50) // anchor
    };

    var iconMy = {
        url: "img/mylocation.png", // url
        origin: new google.maps.Point(0,0), // origin
        anchor: new google.maps.Point(0, 0) // anchor
    };

    document.addEventListener("deviceready", function () {

        $localStorage.typeConnection = $cordovaNetwork.getNetwork()

    }, false);

    var options = {timeout: 10000, enableHighAccuracy: true};

    $ionicModal.fromTemplateUrl('search_modal.html', {

        scope: $scope,

        animation: 'slide-in-up'

    }).then(function(modal) {

        $scope.modal = modal;

    });

    vm.markers = [{},{},{}];

    vm.markersAdmin = [{},{}];

    vm.latitude = 0;

    vm.longitude = 0;

    var map;

    var overlay;

    $scope.showUrban = false;
    console.log($scope.showUrban);

    vm.initialize = function() {

        var myLatlng = new google.maps.LatLng(-23.5498723, -46.6361756);

        var mapOptions = {
            center: myLatlng,
            zoom: 16,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true,
            styles: [
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                {
                    "color": "#e9e9e9"
                },
                {
                    "lightness": 17
                }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "geometry",
                "stylers": [
                {
                    "color": "#f5f5f5"
                },
                {
                    "lightness": 20
                }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [
                {
                    "color": "#ffffff"
                },
                {
                    "lightness": 17
                }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [
                {
                    "color": "#ffffff"
                },
                {
                    "lightness": 29
                },
                {
                    "weight": 0.2
                }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [
                {
                    "color": "#ffffff"
                },
                {
                    "lightness": 18
                }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "geometry",
                "stylers": [
                {
                    "color": "#ffffff"
                },
                {
                    "lightness": 16
                }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [
                {
                    "color": "#f5f5f5"
                },
                {
                    "lightness": 21
                }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [
                {
                    "color": "#dedede"
                },
                {
                    "lightness": 21
                }
                ]
            },
            {
                "elementType": "labels.text.stroke",
                "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "color": "#ffffff"
                },
                {
                    "lightness": 16
                }
                ]
            },
            {
                "elementType": "labels.text.fill",
                "stylers": [
                {
                    "saturation": 36
                },
                {
                    "color": "#333333"
                },
                {
                    "lightness": 40
                }
                ]
            },
            {
                "elementType": "labels.icon",
                "stylers": [
                {
                    "visibility": "off"
                }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "geometry",
                "stylers": [
                {
                    "color": "#f2f2f2"
                },
                {
                    "lightness": 19
                }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.fill",
                "stylers": [
                {
                    "color": "#fefefe"
                },
                {
                    "lightness": 20
                }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [
                {
                    "color": "#fefefe"
                },
                {
                    "lightness": 17
                },
                {
                    "weight": 1.2
                }
                ]
            }
            ]
        };

        map = new google.maps.Map(document.getElementById("map"), mapOptions);

        google.maps.event.trigger(map, "resize");

        vm.myLocation();

    }

    vm.setMapOnAll = function (mapSet) {
        for (var i = 0; i < vm.markers.length; i++) {
            if(vm.markers[i].visible){
                vm.markers[i].setMap(mapSet);
            } 
        }
        for (var i = 0; i < vm.markersAdmin.length; i++) {
            if(vm.markersAdmin[i].visible){
                vm.markersAdmin[i].setMap(mapSet);
            } 
        }
    }

    vm.myLocation = function(){

        $ionicLoading.show({
            content: 'Carregando...',
            animation: 'fade-in',
            noBackdrop: false
        });

        $cordovaGeolocation.getCurrentPosition(options).then(function(position){

            vm.setMapOnAll(null);

            map.setCenter(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));

            map.setZoom(15);

            vm.latitude = position.coords.latitude;

            vm.longitude = position.coords.longitude;

            vm.markers[0] = new google.maps.Marker({
                position: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
                map: map,
                icon: iconMy
            });

            vm.myPosition = {location: {lat: position.coords.latitude, lng: position.coords.longitude, description: "Meu local"}}

            vm.setMapOnAll(map);

            vm.placeholderOrigin = "Meu local";

            $ionicLoading.hide();

        }, function(error){

            $ionicLoading.hide();

             /*$ionicPopup.alert({

                title: 'GPS nÃÂ£o encontrado',

                template: 'Habilite o GPS do seu dispositivo para que melhor possamos lhe atender.'

            }); */ 

        });

    }

    vm.goHome = function(){
        $window.location.href = '#/app/principal';
    }

    vm.search = function(){

        if(!vm.fromLocation.lat || !vm.fromLocation.lng){

            $ionicPopup.alert({

                title: 'Origem não informada',

                template: 'É necessário selecionar uma origem para continuar a pesquisa.'

            });

        } else if(!vm.toLocation.lat || !vm.toLocation.lng){

            $ionicPopup.alert({

                title: 'Destino não informado',

                template: 'É necessário selecionar um destino para continuar a pesquisa.'

            });

        } else { 

            /*if(window.cordova){
                gaPlugin.trackEvent(function(response){console.log(response)}, function(err){console.log(err.message)}, "COMPARAR", "COMPARAR", "COMPARAR", 1);
            }*/
            ga_storage._trackEvent("COMPARAR", "COMPARAR", "COMPARAR", 1)

            $ionicLoading.show({
                content: 'Carregando...',
                animation: 'fade-in',
                noBackdrop: false
            });

            if(vm.history.length > 4){

                vm.history.splice(0, 2)

            }else if(vm.history.length > 3){

                vm.history.splice(0, 1)

            }

            $localStorage.history = vm.history;

            $localStorage.history.push(vm.fromLocation)

            $localStorage.history.push(vm.toLocation)

            Estimates.getDriving(vm.fromLocation, vm.toLocation).then(function(driving){

                try{

                    if(vm.from.trim() == ''){

                        $localStorage.from = vm.placeholderOrigin;

                    }else{

                        $localStorage.from = vm.from.trim();

                    }

                    if(vm.to.trim() == ''){

                        $localStorage.to = vm.placeholderTo;

                    }else{

                        $localStorage.to = vm.to.trim();

                    }

                    vm.duration = driving.rows[0].elements[0].duration.text;

                    vm.distance = driving.rows[0].elements[0].distance.text;

                    Estimates.search(vm.fromLocation, vm.toLocation, $localStorage.typeConnection, vm.distance, vm.duration).then(function(response){

                        $ionicLoading.hide();

                        if(!response || !response.records || response.records == 0){

                            $ionicLoading.hide();

                            $ionicPopup.alert({

                                title: 'Unidade não encontrada',

                                template: 'Não encontramos nenhuma unidade disponí­vel para a rota selecionada.'

                            });

                        }else{

                            if(!vm.toLocation.number || vm.toLocation.number.trim() == ""){
                                vm.toLocation.number = "0";
                            }

                            if(!vm.fromLocation.number || vm.fromLocation.number.trim() == ""){
                                vm.fromLocation.number = "0";
                            }

                            response.records.forEach(function(e){

                                if(e.id == 1){

                                    e.image = "img/players/uber_45.jpg"
                                    
                                    if(!vm.uber){

                                        if(ionic.Platform.isIOS() || ionic.Platform.isIPad()){

                                            e.url_android = 'https://itunes.apple.com/br/app/uber/id368677368?mt=8';

                                        }else if(ionic.Platform.isAndroid()){

                                            e.url_android = 'https://play.google.com/store/apps/details?id=com.ubercab&hl=pt_BR';

                                        }else{

                                            e.url_android = 'https://play.google.com/store/apps/details?id=com.ubercab&hl=pt_BR';

                                        }

                                    } 
                                    

                                }else if(e.id == 2){

                                   e.image = "img/players/cabify_45.jpg";

                                    /*if(!vm.cabify){

                                        if(ionic.Platform.isIOS() || ionic.Platform.isIPad()){

                                            e.url = 'https://itunes.apple.com/us/app/cabify-your-private-driver/id476087442?mt=8';

                                        }else if(ionic.Platform.isAndroid()){

                                            e.url = 'https://play.google.com/store/apps/details?id=com.cabify.rider&hl=pt_BR';

                                        }else{

                                            e.url = 'https://play.google.com/store/apps/details?id=com.cabify.rider&hl=pt_BR';
                                            
                                        }

                                    } */

                                }if(e.id == 3){

                                    e.image = "img/players/99taxi_45.jpg"

                                    /*if(!vm.taxis99){

                                       if(ionic.Platform.isIOS() || ionic.Platform.isIPad()){

                                            e.url = 'https://itunes.apple.com/br/app/99/id553663691?mt=8';

                                        }else if(ionic.Platform.isAndroid()){

                                            e.url = 'https://play.google.com/store/apps/details?id=com.taxis99&hl=pt_BR';

                                        }else{

                                            e.url = 'https://play.google.com/store/apps/details?id=com.taxis99&hl=pt_BR';
                                            
                                        }

                                    } */

                                }if(e.id == 4){

                                    e.image = "img/players/easytaxi_45.jpg";

                                    if(!vm.easy){

                                        if(ionic.Platform.isIOS() || ionic.Platform.isIPad()){

                                            e.url_android = 'https://itunes.apple.com/br/app/easy-viaje-do-seu-jeito/id567264775?mt=8';

                                        }else if(ionic.Platform.isAndroid()){

                                            e.url_android = 'https://play.google.com/store/apps/details?id=br.com.easytaxi&hl=pt_BR';

                                        }else{

                                            e.url_android = 'https://play.google.com/store/apps/details?id=br.com.easytaxi&hl=pt_BR';
                                            
                                        }

                                    } 

                                }

                                if(e.id == 5){

                                    e.image = "img/players/yetgo.jpg";

                                    console.log(vm.yetgo)

                                    if(!vm.yetgo){

                                        if(ionic.Platform.isIOS() || ionic.Platform.isIPad()){

                                            e.url_android = 'https://itunes.apple.com/br/app/yet-go/id1181035299?mt=8';

                                        }else if(ionic.Platform.isAndroid()){

                                            e.url_android = 'https://play.google.com/store/apps/details?id=com.br.yetgo&hl=pt_BR';

                                        }else{

                                            e.url_android = 'https://play.google.com/store/apps/details?id=com.br.yetgo&hl=pt_BR';
                                            
                                        }

                                    } 

                                }

                            });

vm.checkNotification(vm.fromLocation.state)

vm.checkNotification(vm.toLocation.state)

vm.checkNotification(vm.fromLocation.city.replace(" ",""))

vm.checkNotification(vm.toLocation.city.replace(" ",""))

vm.resultShow = true;

vm.searchShow = false;

vm.list = response.records;

$localStorage.isback = true;

}

}, function(error){

    $ionicLoading.hide();

    $log.error(error);

                        /*$ionicPopup.alert({

                            title: 'Erro nÃƒÃ‚Â£o esperado',

                            template: 'Obtivemos um erro ao acessar nossos serviÃƒÃ‚Â§os. Verifique sua conexÃƒÃ‚Â£o com a internet e tente novamente.'

                        }); */

                    });

}catch(err){

    $ionicLoading.hide();

    $log.error(err);

                   /* $ionicPopup.alert({

                        title: 'Erro nÃƒÃ‚Â£o esperado',

                        template: 'Obtivemos um erro ao acessar os serviÃƒÃ‚Â§os do Google. Verifique sua conexÃƒÃ‚Â£o com a internet e tente novamente.'

                    });  */

                }

            }, function(error){

                $ionicLoading.hide();

                $log.error(error);

                /*$ionicPopup.alert({

                    title: 'Erro nÃ£o esperado',

                    template: 'Obtivemos um erro ao acessar os serviÃƒÃ‚Â§os do Google. Verifique sua conexÃƒÃ‚Â£o com a internet e tente novamente.'

                });  */

            });

}

}


vm.openUrl = function(result){

    var title = '';

    title = result.name;



    var template = "";

    if(result.multiplier && result.multiplier > 1){

        template += '<br /><br /><span style="color: black; font-size: 17px; font-weight: bold;">Tarifa Dinâmica</span><br /></div>';

        template += '<br /><div width="90%" style="text-align: center;"><table><tr><td width="80%"><span style="color: #ffa900;;font-size: 18px; font-weight: bold;">Multiplicador '+result.multiplier+'X sobre o valor total da corrida</span></td><td style="vertical-align: bottom; text-align: left;"><img src="img/raio_dinamico.png" style="margin-top: -5px;" width="36px"/></td></tr></table></div>';

    }else{

        if(result.modality.promotion.id && result.modality.promotion.id > 0){

            title = 'Encontramos um desconto para você';

            template += '<br />' +

            '<table style="margin: 0 auto 0 auto">' +
            '<tr>' +
            '<td >' +
            '<img src="img/newimages/logo_vah_azul.png" width="45px" style="margin-right: 7px;"/>' +
            '</td>' +
            '<td style="vertical-align: middle;">' +
            '<i class="icon ion-android-arrow-forward" style="font-size: 36px; color: gray; margin-left: 5px;"></i>' +
            '</td>';

            if(result.id == 1){

                template += '<td>' +
                '<img src="img/players/uber_45.png" style="margin-left: 10px;"/>' + 
                '</td>';

            }else if(result.id == 2){

                template += '<td>' +
                '<img src="img/players/cabify_45.jpg" style="margin-left: 10px;"/>' +
                '</td>';

            }else if(result.id == 3){

                template += '<td>' +
                '<img src="img/players/99taxi_45.png" style="margin-left: 10px;"/>' +
                '</td>';

            }else if(result.id == 4){

                template += '<td>' +
                '<img src="img/players/easy_45.png" style="margin-left: 10px;"/>' +
                '</td>';

            }

            template += '<td style="vertical-align: middle;">' +
            '<span style="font-size: 25px; color: gray; text-transform: lowercase; margin-left: 5px;">' + result.modality.name + '</span>'
            '</td>';

            template += '</tr>' +
            '</table>';

            

            template += '<span style=" line-height: 14px; color:gray;top: 108px;left: 0px;font-size: 11px;">Você será redirecionado para o aplicativo correspondente e lá poderá concluir sua chamada. Obrigado!</span><br />';
            template += '<br /><span style="color: gray;top: 140px;left: 50px;font-size:14px; margin-top:10px; font-weight: bold;"><b>CÓDIGO:</span><span style="color: #004180;top: 140px;left: 50px;font-size:14px; margin-top:10px; font-weight: bold;"> '+result.modality.promotion.exibition_name+'</span></b><br />' ;
            template += '<span style="color: gray;top: 140px;left: 50px;font-size:14px; margin-top:10px; font-weight: bold;"><b>DESCONTO:</span><span style="color: #004180;top: 140px;left: 50px;font-size:14px; margin-top:10px; font-weight: bold;"> '+result.modality.promotion.off +'%</span></b><br />' ;
            template += '<span style="color: gray;top: 140px;left: 50px;font-size:14px; margin-top:10px; font-weight: bold;"><b>LIMITE POR CORRIDA:</span><span style="color: #004180;top: 140px;left: 50px;font-size:14px; margin-top:10px; font-weight: bold;"> R$'+result.modality.promotion.limit_off+'</span></b><br />' ;
            
            if(result.modality.promotion.end_at && result.modality.promotion.end_at.trim() != ""){
                
                var formatedDate = result.modality.promotion.end_at.substring(8, 10) + "/" + result.modality.promotion.end_at.substring(5, 7) +  "/" + result.modality.promotion.end_at.substring(0, 4);

                template += '<span style="color: gray;top: 140px;left: 50px;font-size:14px; margin-top:10px; font-weight: bold;"><b>VALIDADE:</span><span style="color: #004180;top: 140px;left: 50px;font-size:14px; margin-top:10px; font-weight: bold;"> '+formatedDate+'</span></b><br />'  ;             

            }

            template += '<br />' + result.modality.promotion.description + '<br /></div>';

        }else{

            template += '<br />' +

            '<table style="margin: 0 auto 0 auto">' +
            '<tr>' +
            '<td >' +
            '<img src="img/newimages/logo_vah_azul.png" width="45px" style="margin-right: 7px;"/>' +
            '</td>' +
            '<td style="vertical-align: middle;">' +
            '<i class="icon ion-android-arrow-forward" style="font-size: 36px; color: gray; margin-left: 5px;"></i>' +
            '</td>';

            if(result.id == 1){

                template += '<td>' +
                '<img src="img/players/uber_45.png" style="margin-left: 10px;"/>' + 
                '</td>';

            }else if(result.id == 2){

                template += '<td>' +
                '<img src="img/players/cabify_45.jpg" style="margin-left: 10px;"/>' +
                '</td>';

            }else if(result.id == 3){

                template += '<td>' +
                '<img src="img/players/99taxi_45.png" style="margin-left: 10px;"/>' +
                '</td>';

            }else if(result.id == 4){

                template += '<td>' +
                '<img src="img/players/easy_45.png" style="margin-left: 10px;"/>' +
                '</td>';

            }

            template += '<td style="vertical-align: middle;">' +
            '<span style="font-size: 25px; color: gray; text-transform: lowercase; margin-left: 5px;">' + result.modality.name + '</span>'
            '</td>';


            template += '</tr>' +
            '</table><br />';

            template += '<span style=" line-height: 14px; color:gray;top: 108px;left: 0px;font-size: 11px;">Você será redirecionado para o aplicativo correspondente e lá poderá concluir sua chamada. Obrigado!</span><br />';

        }    

    }

    var confirmPopup = undefined;

    if(result.modality.promotion.id && result.modality.promotion.id > 0){

        confirmPopup = $ionicPopup.confirm({
            title: title,
            template: template,
            cancelText: 'Cancelar',
            okText: 'IR'
        });

    }else{

        confirmPopup = $ionicPopup.confirm({
            title: title,
            cssClass: 'custom2-class',
            template: template,
            cancelText: 'Cancelar',
            okText: 'IR'
        });

    }



    confirmPopup.then(function(res) {

        if(res) {

            var promo = "";

            if(result.modality.promotion.id && result.modality.promotion.id > 0){

                promo = result.modality.promotion.exibition_name;

            }

            var store = 1;

            if(result.url_android.indexOf('https') >= 0 && result.url_android.indexOf('app.adjust.com') == -1){

                store = 2

            }

            Estimates.saveResult(result, promo, store).then(function(response){

            }, function(error){

                $log.error(error);

            });

            try{

                window.open(result.url_android,'_system');

                if(window.cordova){

                    if(result.id == 1){
                        if(vm.uber){
                                    //gaPlugin.trackEvent(function(response){console.log(response)}, function(err){console.log(err.message)}, 'Uber', promo, 'App', 1);
                                    ga_storage._trackEvent('Uber', promo, 'App', 1)
                                }else{
                                    //gaPlugin.trackEvent(function(response){console.log(response)}, function(err){console.log(err.message)}, 'Uber', promo, 'Loja', 1);
                                    ga_storage._trackEvent('Uber', promo, 'Loja', 1)
                                }
                            }


                            if(result.id == 2){
                                if(vm.cabify){
                                    //gaPlugin.trackEvent(function(response){console.log(response)}, function(err){console.log(err.message)}, 'Cabify', promo, 'App', 1);
                                    ga_storage._trackEvent('Cabify', promo, 'App', 1)
                                }else{
                                    //gaPlugin.trackEvent(function(response){console.log(response)}, function(err){console.log(err.message)}, 'Cabify', promo, 'Loja', 1);
                                    ga_storage._trackEvent('Cabify', promo, 'Loja', 1)
                                }
                            }


                            if(result.id == 3){
                                if(vm.taxis99){
                                    //gaPlugin.trackEvent(function(response){console.log(response)}, function(err){console.log(err.message)}, '99', promo, 'App', 1);
                                    ga_storage._trackEvent('99', promo, 'App', 1)
                                }else{
                                    //gaPlugin.trackEvent(function(response){console.log(response)}, function(err){console.log(err.message)}, '99', promo, 'Loja', 1);
                                    ga_storage._trackEvent('99', promo, 'Loja', 1)
                                }
                            }


                            if(result.id == 4){
                                if(vm.easy){
                                    //gaPlugin.trackEvent(function(response){console.log(response)}, function(err){console.log(err.message)}, 'Easy', promo, 'App', 1);
                                    ga_storage._trackEvent('Easy', promo, 'App', 1)
                                }else{
                                    //gaPlugin.trackEvent(function(response){console.log(response)}, function(err){console.log(err.message)}, 'Easy', promo, 'Loja', 1);
                                    ga_storage._trackEvent('Easy', promo, 'Loja', 1)
                                }
                            }

                            if(result.id == 5){
                                if(vm.yetgo){
                                    //gaPlugin.trackEvent(function(response){console.log(response)}, function(err){console.log(err.message)}, 'Easy', promo, 'App', 1);
                                    ga_storage._trackEvent('YetGo', promo, 'App', 1)
                                }else{
                                    //gaPlugin.trackEvent(function(response){console.log(response)}, function(err){console.log(err.message)}, 'Easy', promo, 'Loja', 1);
                                    ga_storage._trackEvent('YetGo', promo, 'Loja', 1)
                                }
                            }

                        }
                    }catch(err){

                        $ionicPopup.alert({

                            title: 'Aplicativo não aberto',

                            template: 'Infelizmente não conseguimos abrir o aplicativo cliente.'

                        });

                    }
                } else {

                    ga_storage._trackEvent('POPUP RESULTADO FECHADA', result.name, promo, 1)

                }
            });
}


vm.checkNotification = function(topic){

    Estimates.saveNotificationId(topic.toLowerCase());
}



vm.invertOriginDest = function(){

    var locationAux = vm.fromLocation;
    var addressAux = vm.from;

    vm.fromLocation = vm.toLocation;
    vm.from = vm.to;

    vm.toLocation = locationAux;
    vm.to = addressAux;

    vm.clearDirections();

    vm.routeDirection(true);

}


vm.openModal = function(origin) {

    vm.origin = origin;

    if(origin == 1 && vm.from.trim().length > 0){
        vm.searchFrom = vm.from.trim();
    }else if(origin == 2 && vm.to.trim().length > 0){
        vm.searchFrom = vm.to.trim();
    }else{
        vm.searchFrom = '';
    }

    vm.searchShow = false;

    vm.adminShow = false;

    $scope.modal.show();

};

$scope.closeModal = function() {

    if(vm.origin == 1 || vm.origin == 2){

        vm.adminShow = false;
        vm.resultShow = false;
        $timeout(function(){ 
            vm.searchShow = true;
        }, 300);


    }else if(vm.origin == 3 || vm.origin == 4){

        vm.resultShow = false;
        vm.searchShow = false;
        $timeout(function(){ 
            vm.adminShow = true;
        }, 300);

    }

    $scope.modal.hide();

};

var count = 0;

var arraySearch = [];

vm.getPlaces = function(inputValue){

    var contActual = (JSON.parse(JSON.stringify(count++)));

    arraySearch[contActual] = inputValue;

    $timeout(function(){ 

        if(contActual == (arraySearch.length - 1)){

            arraySearch = [];

            count = 0;

            if(inputValue.length > 4){

                if(vm.latitude == 0){

                    $cordovaGeolocation.getCurrentPosition(options).then(function(position){

                        Estimates.nearbysearch(inputValue, position.coords.latitude, position.coords.longitude).then(function(response){


                            if(response && response.predictions && response.predictions.length > 0){

                                vm.resultPlaces = response.predictions;

                            }else{

                                vm.resultPlaces = [];

                            }

                        }, function(error){

                            Estimates.places(inputValue).then(function(response){

                                if(response && response.predictions && response.predictions.length > 0){

                                    vm.resultPlaces = response.predictions;

                                }else{

                                    vm.resultPlaces = [];

                                }

                            }, function(error){

                                vm.resultPlaces = [];

                            });

                        });

                    }, function(error){

                        Estimates.places(inputValue).then(function(response){

                            if(response && response.predictions && response.predictions.length > 0){

                                vm.resultPlaces = response.predictions;

                            }else{

                                vm.resultPlaces = [];

                            }

                        }, function(error){

                            vm.resultPlaces = [];

                        });

                    });

                }else{

                   Estimates.nearbysearch(inputValue, vm.latitude, vm.longitude).then(function(response){

                    if(response && response.predictions && response.predictions.length > 0){

                        vm.resultPlaces = response.predictions;

                    }else{

                        vm.resultPlaces = [];

                    }

                }, function(error){

                    Estimates.places(inputValue).then(function(response){

                        if(response && response.predictions && response.predictions.length > 0){

                            vm.resultPlaces = response.predictions;

                        }else{

                            vm.resultPlaces = [];

                        }

                    }, function(error){

                        vm.resultPlaces = [];

                    });

                });

               }

           }else{

            vm.resultPlaces = [];

        }
    }

}, 600);

}

vm.clearDirections = function(){
    vm.listDirections.forEach(function(e){
        e.setMap(null);
    });
}

    vm.routeDirection = function(issearch){

        

        if(vm.fromLocation.lat && vm.toLocation.lat){

            vm.setMapOnAll(null);

            map.setZoom(17);

            //create empty LatLngBounds object
            var bounds = new google.maps.LatLngBounds();

            var infowindow = new google.maps.InfoWindow();    
           
            vm.markers[0] = new google.maps.Marker({
                position: new google.maps.LatLng(vm.fromLocation.lat, vm.fromLocation.lng),
                map: map,
                icon: iconStart
            });

            bounds.extend(vm.markers[0].position);

            google.maps.event.addListener(vm.markers[0], 'click', (function (marker, i) {
                return function () {
                    infowindow.setContent(locations[i][0]);
                    infowindow.open(map, marker);
                }
            })(vm.markers[0], 0));



            vm.markers[1] = new google.maps.Marker({
                position: new google.maps.LatLng(vm.toLocation.lat, vm.toLocation.lng),
                map: map,
                icon: iconStart
            });

            bounds.extend(vm.markers[1].position);

            google.maps.event.addListener(vm.markers[1], 'click', (function (marker, i) {
                return function () {
                    infowindow.setContent(locations[i][0]);
                    infowindow.open(map, marker);
                }
            })(vm.markers[1], 1));

            map.fitBounds(bounds);

            var listener = google.maps.event.addListener(map, "idle", function () {
                map.setZoom(map.getZoom() -1);
                if(vm.markers[1].position.lat() > vm.markers[0].position.lat()){
                    map.setCenter(new google.maps.LatLng(vm.markers[1].position.lat(),vm.markers[1].position.lng()));
                }else{
                    map.setCenter(new google.maps.LatLng(vm.markers[0].position.lat(),vm.markers[0].position.lng()));
                }
                google.maps.event.removeListener(listener);
            });

        }else if(issearch){

            var center;
            var offsetX = 0.0;
            var offsetY = 0.25;

            var span = map.getBounds().toSpan();

            var newCenter = { 
                lat: map.getCenter().lat() + span.lat()*offsetY,
                lng: map.getCenter().lng() + span.lng()*offsetX
            };

            map.panTo(newCenter);

        }

        /*if(issearch){

            var center;
            var offsetX = 0.0;
            var offsetY = 0.25;

            var span = map.getBounds().toSpan();

            var newCenter = { 
                lat: map.getCenter().lat() + span.lat()*offsetY,
                lng: map.getCenter().lng() + span.lng()*offsetX
            };

            map.panTo(newCenter);

        } */

    }

    vm.selectMyLocal = function(){

        $ionicLoading.show({
            content: 'Carregando...',
            animation: 'fade-in',
            noBackdrop: false
        });

        $cordovaGeolocation.getCurrentPosition(options).then(function(position){

            vm.setMapOnAll(null);

            map.setCenter(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));

            map.setZoom(15);

            vm.latitude = position.coords.latitude;

            vm.longitude = position.coords.longitude;

            Estimates.getAddressByLatLng(position.coords.latitude, position.coords.longitude).then(function(response){

                if(response.results && response.results.length > 0){

                    var result = response.results[0];

                    var address = "";
                    var district = "";
                    var city = "";
                    var state = "";
                    var zipcode = "";
                    var number = "";
                    var country = "";

                    result.address_components.forEach(function(e){

                        if(e.types && e.types.length > 0){

                            e.types.forEach(function(t){

                                if("street_number" == t){
                                    number = e.short_name;
                                    return;
                                }else if("route"  == t){
                                    address += e.short_name + ", ";
                                    return;
                                }else if("sublocality_level_1" == t){
                                    district = e.short_name;
                                    return;
                                }else if("administrative_area_level_2" == t){
                                    city = e.short_name;
                                    return;
                                }else if("administrative_area_level_1" == t){
                                    state = e.short_name;
                                    return;
                                }else if("postal_code" == t){
                                    zipcode = e.short_name;
                                    return;
                                }else if("country" == t){
                                    country = e.short_name;
                                    return;
                                }

                            });

                        }

                    });

                    var structured_formatting = {main_text: address + number, secondary_text: district + ", " + city + " - " + state + ", " + country}

                    vm.myPosition = {
                        location: {
                            lat: position.coords.latitude, 
                            lng: position.coords.longitude,
                            state: state,
                            city: city,
                            district: district,
                            address: structured_formatting.main_text + ", " + structured_formatting.secondary_text,
                            zipcode: zipcode,
                            number: number,
                            structured_formatting: structured_formatting
                        }, 
                        description: response.results[0].formatted_address
                    };

                }else{

                    vm.myPosition = {location: {lat: position.coords.latitude, lng: position.coords.longitude}, description: "Meu Local"};

                }

                vm.fromLocation = vm.myPosition.location;

                vm.from = vm.myPosition.description;

                map.setCenter(new google.maps.LatLng(vm.fromLocation.lat, vm.fromLocation.lng));

                map.setZoom(15);

                vm.markers[0] = new google.maps.Marker({
                    position: new google.maps.LatLng(vm.fromLocation.lat, vm.fromLocation.lng),
                    map: map,
                    draggable: true,
                    icon: iconStart
                });

                google.maps.event.addListener(
                    vm.markers[0],
                    'drag',
                    function() {
                        vm.fromLocation = {lat: vm.markers[0].position.lat(), lng: vm.markers[0].position.lng()};
                    }
                    );

                vm.clearDirections();

                vm.setMapOnAll(map);

                vm.routeDirection(true);

                $ionicLoading.hide();

            }, function(error){

                $log.error(error);

                $ionicLoading.hide();
                
                vm.myPosition = {location: {lat: position.coords.latitude, lng: position.coords.longitude}, description: "Meu Local"};

                vm.fromLocation = vm.myPosition.location;

                vm.from = vm.myPosition.description;

                map.setCenter(new google.maps.LatLng(vm.fromLocation.lat, vm.fromLocation.lng));

                map.setZoom(15);

                vm.markers[0] = new google.maps.Marker({
                    position: new google.maps.LatLng(vm.fromLocation.lat, vm.fromLocation.lng),
                    map: map,
                    draggable: true,
                    icon: iconStart
                });

                google.maps.event.addListener(
                    vm.markers[0],
                    'drag',
                    function() {
                        vm.fromLocation = {lat: vm.markers[0].position.lat(), lng: vm.markers[0].position.lng()};
                    }
                    );

                vm.clearDirections();

                vm.setMapOnAll(map);

                vm.routeDirection(true);

                $ionicLoading.hide();

            });

}, function(error){

    $ionicLoading.hide();

    $ionicPopup.alert({

        title: 'GPS não encontrado',

        template: 'Habilite o GPS do seu dispositivo para que melhor possamos lhe atender.'

    });

});

};

vm.showAdmin = function(){

    $window.location.href = '#/app/admin';

}

vm.markerHouseJob = function(){

    $ionicLoading.show({
        content: 'Carregando...',
        animation: 'fade-in',
        noBackdrop: false
    });

    $scope.closeModal();

    vm.setMapOnAll(null);

    if(vm.house.address){

        vm.houseLocation = vm.house;

        vm.markersAdmin[0] = new google.maps.Marker({
            position: new google.maps.LatLng(vm.houseLocation.lat, vm.houseLocation.lng),
            map: map,
            draggable: true,
            icon: iconHouse,
            animation: google.maps.Animation.DROP
        });

        google.maps.event.addListener(
            vm.markersAdmin[0],
            'drag',
            function() {
                vm.houseLocation = location;
            }
            );

        map.setCenter(vm.houseLocation);

    }

    if(vm.work.address){

        vm.workLocation = vm.work;

        $ionicLoading.hide();

        vm.markersAdmin[1] = new google.maps.Marker({
            position: new google.maps.LatLng(vm.work.lat, vm.work.lng),
            map: map,
            draggable: true,
            icon: iconJob,
            animation: google.maps.Animation.DROP
        });

        google.maps.event.addListener(
            vm.markersAdmin[1],
            'drag',
            function() {
                vm.workLocation = location;
            }
            );

        map.setCenter(vm.workLocation);

    }

    if(vm.markersAdmin[0].position && vm.markersAdmin[1].position){
        vm.centerMarkers(vm.markersAdmin[0], vm.markersAdmin[1])  
    }

    $ionicLoading.hide();

}

vm.getFuncion = function(){

    vm.showAdmin();

}    

vm.selectAddress = function(place){

    $ionicLoading.show({
        content: 'Carregando...',
        animation: 'fade-in',
        noBackdrop: false
    });

    $scope.closeModal();

    vm.setMapOnAll(null);

    var location = place;

    if(vm.origin == 1){

        vm.searchShow = true;

        vm.from = place.address;

        vm.fromLocation = location;

        vm.markers[0] = new google.maps.Marker({
            position: new google.maps.LatLng(vm.fromLocation.lat, vm.fromLocation.lng),
            map: map,
            draggable: true,
            icon: iconStart,
            animation: google.maps.Animation.DROP
        });

        google.maps.event.addListener(
            vm.markers[0],
            'drag',
            function() {
                vm.fromLocation = location;
            }
            );

        map.setCenter(vm.fromLocation);

        vm.clearDirections();

        vm.setMapOnAll(map);

        vm.routeDirection(false);

    }else if(vm.origin == 2){

        vm.searchShow = true;

        vm.to = place.address;

        vm.toLocation = location;

        vm.markers[1] = new google.maps.Marker({
            position: new google.maps.LatLng(vm.toLocation.lat, vm.toLocation.lng),
            map: map,
            draggable: true,
            icon: iconEnd,
            animation: google.maps.Animation.DROP
        });

        google.maps.event.addListener(
            vm.markers[1],
            'drag',
            function() {
                vm.toLocation = location;
            }
            );

        map.setCenter(vm.toLocation);

        vm.setMapOnAll(map);

        vm.clearDirections();

        vm.routeDirection(false);

    }else if(vm.origin == 3){

        vm.adminShow = true;

        vm.house = location;

        vm.houseLocation = location;

        $localStorage.house = location;

        vm.markersAdmin[0] = new google.maps.Marker({
            position: new google.maps.LatLng(location.lat, location.lng),
            map: map,
            draggable: true,
            icon: iconHouse,
            animation: google.maps.Animation.DROP
        });

        google.maps.event.addListener(
            vm.markersAdmin[0],
            'drag',
            function() {
                vm.houseLocation = location;
            }
            );

        map.setCenter(vm.houseLocation);

        vm.setMapOnAll(null);

        vm.setMapOnAll(map);

    }else if(vm.origin == 4){

        vm.adminShow = true;

        vm.work = location;

        vm.workLocation = location;

        $localStorage.work = location;

        vm.markersAdmin[1] = new google.maps.Marker({
            position: new google.maps.LatLng(vm.workLocation.lat, vm.workLocation.lng),
            map: map,
            draggable: true,
            icon: iconJob,
            animation: google.maps.Animation.DROP
        });

        google.maps.event.addListener(
            vm.markersAdmin[1],
            'drag',
            function() {
                vm.workLocation = location;
            }
            );

        map.setCenter(vm.workLocation);

        vm.setMapOnAll(null);

        vm.setMapOnAll(map);

    }

    $ionicLoading.hide();

}

vm.backSearch = function(){

   $ionicLoading.show({
        content: 'Carregando...',
        animation: 'fade-in',
        noBackdrop: false
    });

   $timeout(function(){ 

        vm.resultShow = false;

        vm.adminShow = false;

        vm.fromLocation = {};

        vm.from = '';

        vm.toLocation = {};

        vm.to = '';

        vm.clearDirections();

        vm.setMapOnAll(null);

        vm.markersAdmin = [{},{}];

        vm.markers = [{},{},{}];

        $ionicLoading.hide();

        vm.iconHeader = "img/icon-car2.png";

        vm.searchShow = true;

    }, 300);

}

vm.centerMarkers = function(marker1, marker2){

    vm.markers = [{},{},{}]

    var bounds = new google.maps.LatLngBounds();
    var infowindow = new google.maps.InfoWindow();    

          //extend the bounds to include each marker's position
          bounds.extend(marker1.position);

          google.maps.event.addListener(marker1, 'click', (function(marker, i) {
            return function() {
                infowindow.setContent(marker1.position);
                infowindow.open(map, marker1);
            }
        })(marker1, 0));

          //extend the bounds to include each marker's position
          bounds.extend(marker2.position);

          google.maps.event.addListener(marker2, 'click', (function(marker, i) {
            return function() {
                infowindow.setContent(marker2.position);
                infowindow.open(map, marker2);
            }
        })(marker2, 1));

        //now fit the map to the newly inclusive bounds
        map.fitBounds(bounds);

        //(optional) restore the zoom level after the map is done scaling
        var listener = google.maps.event.addListener(map, "idle", function () {
            map.setZoom(map.getZoom() -1);
            if(marker2.position.lat() > marker1.position.lat()){
                map.setCenter(new google.maps.LatLng(marker2.position.lat(),marker2.position.lng()));
            }else{
                map.setCenter(new google.maps.LatLng(marker1.position.lat(),marker1.position.lng()));
            }
            google.maps.event.removeListener(listener);
        });

    }

    

    vm.showMultiplierPopup = function(result){

        $ionicPopup.alert({

            title: 'Preço dinâmimco',

            template: result.popup_multiplier

        });

    }

    vm.cleanSearch = function(){
        vm.searchFrom = "";
    }

    vm.back = function(){

        if(vm.resultShow){
            vm.backSearch();
        }else{
            $window.location.href = '#/app/home';
        }

    }

    vm.selectResultPlaces = function(place){

        $ionicLoading.show({
            content: 'Carregando...',
            animation: 'fade-in',
            noBackdrop: false
        });

        $scope.closeModal();

        vm.setMapOnAll(null);

        $log.debug(place);

        Estimates.detailsPlace(place.place_id).then(function(response){

            if(response && response.result){

                var result = response.result;

                var district = "";
                var city = "";
                var state = "";
                var zipcode = "";
                var number = "";

                result.address_components.forEach(function(e){

                    if(e.types && e.types.length > 0){

                        e.types.forEach(function(t){

                            if("sublocality_level_1" == t){
                                district = e.short_name;
                                return;
                            }else if("administrative_area_level_2" == t){
                                city = e.short_name;
                                return;
                            }else if("administrative_area_level_1" == t){
                                state = e.short_name;
                                return;
                            }else if("postal_code" == t){
                                zipcode = e.short_name;
                                return;
                            }else if("street_number" == t){
                                number = e.short_name;
                                return;
                            }

                        });

                    }

                });


                if(number == ""){

                    var numbers = place.description.split(",")

                    if(numbers.length > 0){

                        var numberSplit = (numbers[1].match(/\d+/));

                        if(numberSplit && numberSplit.length > 0){

                            number = numberSplit[0];

                        }

                    }else{

                        number = "";

                    }

                }

                if(place.structured_formatting){
                    if(place.structured_formatting.main_text){
                        place.structured_formatting.main_text = place.structured_formatting.main_text.replace(";", ",");
                    }
                    if(place.structured_formatting.secondary_text){
                        place.structured_formatting.secondary_text = place.structured_formatting.secondary_text.replace(";", ",");
                    }
                }

                var location = {
                    location: {
                        lat: result.geometry.location.lat, 
                        lng: result.geometry.location.lng,
                        state: state,
                        city: city,
                        district: district,
                        address: place.description,
                        zipcode: zipcode,
                        number: number,
                        structured_formatting: place.structured_formatting
                    },  
                    description: result.formatted_address
                };

                if(vm.origin == 1){

                    vm.searchShow = true;

                    vm.from = location.location.address;

                    vm.fromLocation = location.location;

                    vm.markers[0] = new google.maps.Marker({
                        position: new google.maps.LatLng(vm.fromLocation.lat, vm.fromLocation.lng),
                        map: map,
                        draggable: true,
                        icon: iconStart,
                        animation: google.maps.Animation.DROP
                    });

                    google.maps.event.addListener(
                        vm.markers[0],
                        'drag',
                        function() {
                            vm.fromLocation = location;
                        }
                        );

                    map.setCenter(vm.fromLocation);

                    vm.clearDirections();

                    vm.setMapOnAll(map);

                    vm.routeDirection(true);

                }else if(vm.origin == 2){

                    vm.searchShow = true;

                    vm.to = location.location.address;

                    vm.toLocation = location.location;

                    vm.markers[1] = new google.maps.Marker({
                        position: new google.maps.LatLng(vm.toLocation.lat, vm.toLocation.lng),
                        map: map,
                        draggable: true,
                        icon: iconEnd,
                        animation: google.maps.Animation.DROP
                    });

                    google.maps.event.addListener(
                        vm.markers[1],
                        'drag',
                        function() {
                            vm.toLocation = location;
                        }
                        );

                    map.setCenter(vm.toLocation);

                    vm.setMapOnAll(map);

                    vm.clearDirections();

                    vm.routeDirection(true);

                }else if(vm.origin == 3){

                    vm.adminShow = true;

                    vm.house = location.location;

                    vm.houseLocation = location.location;

                    $localStorage.house = location.location;

                    vm.markersAdmin[0] = new google.maps.Marker({
                        position: new google.maps.LatLng(vm.houseLocation.lat, vm.houseLocation.lng),
                        map: map,
                        draggable: true,
                        icon: iconHouse,
                        animation: google.maps.Animation.DROP
                    });

                    google.maps.event.addListener(
                        vm.markersAdmin[0],
                        'drag',
                        function() {
                            vm.houseLocation = location.location;
                        }
                        );

                    map.setCenter(vm.houseLocation);

                    vm.setMapOnAll(null);

                    vm.setMapOnAll(map);

                }else if(vm.origin == 4){

                    vm.adminShow = true;

                    vm.work = location.location;

                    vm.workLocation = location.location;

                    $localStorage.work = location.location;

                    vm.markersAdmin[1] = new google.maps.Marker({
                        position: new google.maps.LatLng(vm.workLocation.lat, vm.workLocation.lng),
                        map: map,
                        draggable: true,
                        icon: iconJob,
                        animation: google.maps.Animation.DROP
                    });

                    google.maps.event.addListener(
                        vm.markersAdmin[1],
                        'drag',
                        function() {
                            vm.workLocation = location.location;
                        }
                        );

                    map.setCenter(vm.workLocation);

                    vm.setMapOnAll(null);

                    vm.setMapOnAll(map);

                }
                
                if(vm.origin == 3 || vm.origin == 4){
                    if(vm.markersAdmin[0].position && vm.markersAdmin[1].position){
                        vm.centerMarkers(vm.markersAdmin[0], vm.markersAdmin[1])  
                    }
                }

            }

            $ionicLoading.hide();

        }, function(error){

            $ionicLoading.hide();

            $log.error(error);

            /* $ionicPopup.alert({

                title: 'Erro nÃÂ£o esperado',

                template: 'Obtivemos um erro ao acessar nossos serviÃÂ§os. Verifique sua conexÃÂ£o com a internet e tente novamente.'

            }); */

        });

}

});