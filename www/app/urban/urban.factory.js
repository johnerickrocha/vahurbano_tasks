angular.module('starter').factory('Estimates', function($http, $log, $localStorage, $q, PlayerModel){

	var header = {'Authorization': '65edc9b5-d134-4c8b-9ce5-ee2c722f4a54'};
	var headerOld = {'Authorization': '65edc9b5-d134-4c8b-9be5-ee2c722f4a54'};

	var keyGoogle = 'AIzaSyDa2yKVjlQEGzrtwdwC9Je7evqNyAsiq6s';

	var urlBackend = 'http://myfirstelasticbeanstalkapplication.g2xtaf7nbz.us-east-1.elasticbeanstalk.com';
	//var urlBackend = 'http://apivahhml.us-east-1.elasticbeanstalk.com';
	//var urlBackend = 'http://localhost:8080';
	//var urlBackend = 'http://apivahstage.us-east-1.elasticbeanstalk.com';

	return {
	    search: function(start_address, end_address, type_connection, distance, duration) {

	    	var data = PlayerModel.getSearchModel(start_address, end_address, type_connection, distance, duration)

	    	var req = {
		        url: urlBackend + '/v2.1/estimates',
		        method: 'POST',
		        data: data,
		        headers: header
		    };

	    	var deferred = $q.defer();

	    	$http(req).then(function(resp) {

				deferred.resolve(resp.data);

			}, function(err) {

				deferred.reject(err);

			});

            return deferred.promise;
        },
        places: function(inputValue) {

        	var params = {key: keyGoogle, input: inputValue, language: 'pt-BR', components: 'country:br'};

	    	var req = {

		        url: 'https://maps.googleapis.com/maps/api/place/autocomplete/json',
		        method: 'GET',
		        params: params
		    };

	    	var deferred = $q.defer();

	    	$http(req).then(function(resp) {

				deferred.resolve(resp.data);

			}, function(err) {

				deferred.reject(err);

			});

            return deferred.promise;
        },
        nearbysearch: function(inputValue, lat, lng) {

        	var params = {key: keyGoogle, input: inputValue, language: 'pt-BR', components: 'country:br', location: lat + ',' + lng, radius: 50000};

	    	var req = {

		        url: 'https://maps.googleapis.com/maps/api/place/autocomplete/json',
		        method: 'GET',
		        params: params
		    };

	    	var deferred = $q.defer();

	    	$http(req).then(function(resp) {

				deferred.resolve(resp.data);

			}, function(err) {

				deferred.reject(err);

			});

            return deferred.promise;
        },
        detailsPlace: function(placeId) {

        	var params = {key: keyGoogle, placeid: placeId};

	    	var req = {

		        url: 'https://maps.googleapis.com/maps/api/place/details/json',
		        method: 'GET',
		        params: params
		    };

	    	var deferred = $q.defer();

	    	$http(req).then(function(resp) {

				deferred.resolve(resp.data);

			}, function(err) {

				deferred.reject(err);

			});

            return deferred.promise;
        },
        getAddressByLatLng: function(lat, lng){

        	var params = {key: keyGoogle, latlng: lat + ',' + lng};

	    	var req = {

		        url: 'https://maps.googleapis.com/maps/api/geocode/json',
		        method: 'GET',
		        params: params
		        
		    };

	    	var deferred = $q.defer();

	    	$http(req).then(function(resp) {

				deferred.resolve(resp.data);

			}, function(err) {

				deferred.reject(err);

			});

            return deferred.promise;

        },
        getDriving: function(start_location, end_location){

        	var params = {
    			key: keyGoogle, 
    			origins: start_location.lat + ',' + start_location.lng, 
    			destinations: end_location.lat + ',' + end_location.lng
    		};

	    	var req = {

		        url: 'https://maps.googleapis.com/maps/api/distancematrix/json',
		        method: 'GET',
		        params: params
		        
		    };

	    	var deferred = $q.defer();

	    	$http(req).then(function(resp) {

				deferred.resolve(resp.data);

			}, function(err) {

				deferred.reject(err);

			});

            return deferred.promise;

        },getLocationByAddress: function(address){

        	var params = {key: keyGoogle, address: address};

	    	var req = {

		        url: 'https://maps.googleapis.com/maps/api/geocode/json',

		        method: 'GET',

		        params: params
		        
		    };

	    	var deferred = $q.defer();

	    	$http(req).then(function(resp) {

				deferred.resolve(resp.data);

			}, function(err) {

				deferred.reject(err);

			});

            return deferred.promise;

        },
        saveNotificationId: function(topic){

        	if(window.cordova){

        		topic = topic.replace(/[á|ã|â|à]/gi, "a");
		      	topic = topic.replace(/[é|ê|è]/gi, "e");
		      	topic = topic.replace(/[í|ì|î]/gi, "i");
		      	topic = topic.replace(/[õ|ò|ó|ô]/gi, "o");
		      	topic = topic.replace(/[ú|ù|û]/gi, "u");
		      	topic = topic.replace(/[ç]/gi, "c");
		      	topic = topic.replace(/[ñ]/gi, "n");
		      	topic = topic.replace(/[á|ã|â]/gi, "a");
		      	topic = topic.replace(/W/gi, "");
		      	topic = topic.split(" ").join("");

	            FCMPlugin.subscribeToTopic(topic)

	        }
        }, 
        saveResult: function(result, promotion, store) {

	    	var req = {
		        url: urlBackend + '/v1/estimate/selected/' + result.uuid,
		        method: 'POST',
		        headers: headerOld,
		        params: {
		        	promotion: promotion,
		        	store: store
		        }
		    };

	    	var deferred = $q.defer();

	    	$http(req).then(function(resp) {

				deferred.resolve(resp.data);

			}, function(err) {

				deferred.reject(err);

			});

            return deferred.promise;
        },
        insertPopup: function(popup) {

	    	$localStorage.lastMessage = popup.id;

	    	return true

        },
        selectMaxPopup: function() {

    		return $localStorage.lastMessage;
        },
        getLastMessage: function(messageId) {

	    	var req = {
		        url: urlBackend + '/v1/messages/start',
		        method: 'GET',
		        headers: headerOld,
		        params: {
		        	lastRecord: messageId
		        }
		    };

	    	var deferred = $q.defer();

	    	$http(req).then(function(resp) {

				deferred.resolve(resp.data);

			}, function(err) {

				deferred.reject(err);

			});

            return deferred.promise;
        }

	};

});