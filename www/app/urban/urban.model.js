angular.module('starter').factory('PlayerModel', function($log){

	var version = "v1.9.0";

	return {
	    getSearchModel: function(start_address, end_address, type_connection, distance, duration) {

	    	ionic.Platform.ready(function(){

    		});		

	    	var device = ionic.Platform.device().uuid;

	    	var id = ionic.Platform.device().uuid;

	    	if(!device){
	    		device = "";
	    	}

	    	if(!id){
	    		id = "";
	    	}

	    	if(!type_connection){
	    		type_connection = "";
	    	}

		    var device = {
		    	"id": id,
		    	"operation_system": ionic.Platform.platform(),
		    	"operation_system_version" : ionic.Platform.version() + "",
		    	"device" : device,
		    	"type_connection" : type_connection,
		    	"version_app": version
		    };

		    var start =  {
				"lat": start_address.lat,
				"lng": start_address.lng,
				"address": start_address.address,
				"district": start_address.district,
				"city": start_address.city,
				"state": start_address.state,
				"zipcode": start_address.zipcode
			};

			var end =  {
				"lat": end_address.lat,
				"lng": end_address.lng,
				"address": end_address.address,
				"district": end_address.district,
				"city": end_address.city,
				"state": end_address.state,
				"zipcode": end_address.zipcode
			};

			console.log(distance)

			var model = {
	    		device: device,
	    		start: start,
	    		end: end,
	    		distance: parseInt(parseFloat(distance.replace("Km", "").replace("KM", "").replace("kM", "").replace(" ", "")) * 1000),
	    		duration: parseInt(duration)
	    	}

	    	return model;

        }

	};

});