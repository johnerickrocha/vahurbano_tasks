angular.module('starter')
    .controller('HomeCtrl', function($window, $state, WebView, $cordovaInAppBrowser, $rootScope,$localStorage) {
        var vm = this;

        $localStorage.urban = false;

        $localStorage.aereo = false;

        $localStorage.hotel = false;

        $localStorage.locacao = false;
        
        $localStorage.pacotes = false;
        
        vm.fnUrban = function() {
             $window.location.href = '#/app/urban';
        }
        
        vm.fnAerial = function() {
            WebView.fnAerial();
        };

        vm.fnHotel = function() {

            WebView.fnHotel();
        };

        vm.fnRental = function() {
            WebView.fnRental();
        };

        vm.fnPacotes = function() {
            WebView.fnPacotes();
        };

 });
      