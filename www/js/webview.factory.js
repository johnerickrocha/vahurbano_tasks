angular.module('starter')
    .factory('WebView', function($window, $ionicLoading, $cordovaSpinnerDialog, $timeout) {
        var options = {
                statusbar: {
                        color: '#ffffff'
                    },
                    toolbar: {
                        height: 44,
                        color: '#00488c',
                        wwwImage: ''
                    },
                    title: {
                        color: '#ffffff',
                        showPageTitle: false
                    },
                    closeButton: {
                        wwwImage: 'img/home2.png',
                        wwwImageDensity: 4,
                        align: 'right',
                        event: 'closePressed'  
                    },
                    backButton: {
                        wwwImage: 'img/back.png',
                        wwwImageDensity: 4, 
                        align: 'left',
                        event: 'closePressed'
                    }
            },
            cssStart = {
                code:   '.InlineHeader{display: none;}' +
                        '.loader-main {position: absolute; display: table; ' + 
                        'top: 0; left: 0; height: 100vh;  width: 100%; ' + 
                        'padding-top: 150px; z-index: 1000; text-align: center; ' + 
                        'background-color: #fff;} ' + 
                        '.loader {display: inline-block; vertical-align: middle; ' +
                        'border: 16px solid #00488c; border-top: 16px solid #fff; ' +
                        'border-bottom: 2px solid #fff; border-radius: 50%; ' +
                        'width: 120px; height: 120px; animation: spin 2s linear infinite;} ' +
                        '.loader-label {color: #00488c; font-size: 1.2em; padding-top: 20px;} ' +
                        '@keyframes spin {0% { transform: rotate(0deg); } 100% { transform: rotate(360deg);}} '
            },
            scriptStart = {
                code:   "var loaderMain = document.createElement('DIV'), " +
                        "loader = document.createElement('DIV'), " +
                         "loaderLabel = document.createElement('DIV'); " +
                        "loaderLabel.innerHTML = 'Carregando...'; " +
                        "loaderMain.className = 'teste-teste-teste'; " +
                        /*"loader.className = 'loader'; " +
                        "loaderLabel.className = 'loader-label'; " +
                        "loaderMain.appendChild(loader); " + 
                        "loaderMain.appendChild(loaderLabel); " +*/
                        "document.getElementsByTagName('BODY')[0].appendChild(loaderMain); " +
                        /*"document.getElementsByClassName('InlineHeader')[0].style.display = 'none'; "*/""
            },
            cssStop = {
                code:   '.InlineHeader {display: none;} ' + 
                        '.fdTopNavLinksCol {display: none;} ' + 
                        '.fdFormBannerLinksContainer {display: none;} ' + 
                    //    '.fdWidgets {display: none;} ' + 
                        '.Footer {display: none;} ' +    
                        '.hearder-app {padding: 10px 0px; width: 100%; background-color: #00488c; text-align: center; font-size: 2em; font-weight: 600; color: #FFFFFF;} ' +
                        '.item-header-left {float:left;} ' +          
                        '.btn-close {background: rgba(0, 0, 0, 0); height: 40px; border: 2px solid #79ea6c; border-radius: 5px; font-size: 0.5em; color: #79ea6c; margin-left: 3px;} ' +
                        '.SearchSummary  {display: none }' +
                        'SearchSummary--shortLocations {display: none }'+
                        'resultsLoadingDialog {background-color: white}'+
                        'r9-dialog {display: none}'+
                        '.rpSortOptions { display: none !important }'
            };
        
        _fnAerial = function() {
            

            document.addEventListener("deviceready", function () {

                options.toolbar.wwwImage = 'img/logo_vahaereo2.png';
                var ref = cordova.ThemeableBrowser.open('https://www.kayak.com.br/flights?a=360meridianos&p=1%2F%2F360meridianos', '_blank', options);

                ref.addEventListener('loadstart', function(){

                    navigator.splashscreen.show();

                    $timeout(function () {
                        SpinnerPlugin.activityStart("Carregando vahaéreo...", { dimBackground: true });
                    }, 500);

                    
                    //$cordovaSpinnerDialog.show("title","message", true);
                    
                    ref.insertCSS(cssStart);
                    // ref.executeScript(scriptStart);
                });

                ref.addEventListener('loadstop', function(){
                    ref.insertCSS(cssStop);
                    //$ionicLoading.hide();
                    SpinnerPlugin.activityStop();
                    navigator.splashscreen.hide();
                    //$cordovaSpinnerDialog.hide();
                });

                ref.addEventListener('loaderror', function(){
                    alert('Desculpe pelos transtornos. Verifique se está ativado WIFI ou pacote de dados e tente novamente.');
                    ref.close();
                });

                ref.addEventListener('exit', function(){
                    if($window.location.href.indexOf('app/urban') > -1) {
                       $window.location.href = "#/app/home";
                    }
                });

            }, false);

        };

        _fnHotel = function() {


            document.addEventListener("deviceready", function () {

                options.toolbar.wwwImage = 'img/logo_vahhoteis2.png';
                var ref = cordova.ThemeableBrowser.open('https://www.kayak.com.br/hotels?a=360meridianos&p=1%2F%2F360meridianos', '_blank', options);

                ref.addEventListener('loadstart', function(){

                    navigator.splashscreen.show();

                    $timeout(function () {
                        SpinnerPlugin.activityStart("Carregando vahhotéis...", { dimBackground: true });
                    }, 500);

                    
                    //$cordovaSpinnerDialog.show("title","message", true);
                    
                    ref.insertCSS(cssStart);
                    // ref.executeScript(scriptStart);
                });

                ref.addEventListener('loadstop', function(){
                    ref.insertCSS(cssStop);
                    //$ionicLoading.hide();
                    SpinnerPlugin.activityStop();
                    navigator.splashscreen.hide();
                    //$cordovaSpinnerDialog.hide();
                });

                ref.addEventListener('loaderror', function(){
                    alert('Desculpe pelos transtornos. Verifique se está ativado WIFI ou pacote de dados e tente novamente.');
                    ref.close();
                });

                ref.addEventListener('exit', function(){
                    $window.location.href = "#/app/home";
                    
                });

            }, false);

        };

        _fnRental = function() {

            document.addEventListener("deviceready", function () {

                options.toolbar.wwwImage = 'img/logo_vahlocacao2.png';
                var ref = cordova.ThemeableBrowser.open('https://www.kayak.com.br/cars?a=360meridianos&p=1%2F%2F360meridianos', '_blank', options);

                ref.addEventListener('loadstart', function(){

                    navigator.splashscreen.show();

                    $timeout(function () {
                        SpinnerPlugin.activityStart("Carregando vahlocação...", { dimBackground: true });
                    }, 500);

                    
                    //$cordovaSpinnerDialog.show("title","message", true);
                    
                    ref.insertCSS(cssStart);
                    // ref.executeScript(scriptStart);
                });

                ref.addEventListener('loadstop', function(){
                    ref.insertCSS(cssStop);
                    //$ionicLoading.hide();
                    SpinnerPlugin.activityStop();
                    navigator.splashscreen.hide();
                    //$cordovaSpinnerDialog.hide();
                });

                ref.addEventListener('loaderror', function(){
                    alert('Desculpe pelos transtornos. Verifique se está ativado WIFI ou pacote de dados e tente novamente.');
                    ref.close();
                });

                ref.addEventListener('exit', function(){
                    if($window.location.href.indexOf('app/urban') > -1) {
                       $window.location.href = "#/app/home";
                    }
                });
            }, false);
        };

        _fnPacotes = function() {

            document.addEventListener("deviceready", function () {

                options.toolbar.wwwImage = 'img/logo_vahpacotes.png';
                var ref = cordova.ThemeableBrowser.open('https://www.kayak.com.br/packagetours', '_blank', options);

                ref.addEventListener('loadstart', function(){

                    navigator.splashscreen.show();

                    $timeout(function () {
                       SpinnerPlugin.activityStart("Carregando vahpacotes...", { dimBackground: true });
                    }, 500);

                    
                    //$cordovaSpinnerDialog.show("title","message", true);
                    
                    ref.insertCSS(cssStart);
                    // ref.executeScript(scriptStart);
                });

                ref.addEventListener('loadstop', function(){
                    ref.insertCSS(cssStop);
                    //$ionicLoading.hide();
                    SpinnerPlugin.activityStop();
                    navigator.splashscreen.hide();
                    //$cordovaSpinnerDialog.hide();
                });

                ref.addEventListener('loaderror', function(){
                    alert('Desculpe pelos transtornos. Verifique se está ativado WIFI ou pacote de dados e tente novamente.');
                    ref.close();
                });

                ref.addEventListener('exit', function(){
                    if($window.location.href.indexOf('app/urban') > -1) {
                       $window.location.href = "#/app/home";
                    }
                });
            }, false);
        };

        return {
            fnAerial: _fnAerial,
            fnHotel: _fnHotel,
            fnRental: _fnRental,
            fnPacotes: _fnPacotes
        }
    });