angular.module('starter')
	.controller('AppCtrl', function($log, $scope, $window, $localStorage, WebView, $ionicPopup, $ionicPlatform){
		var vm = this;
		
		$scope.shownGroup = null;

		if(!$localStorage.removedCacheTwoLines5){
			$localStorage.history = [];
			$localStorage.house = {};
			$localStorage.work = {};
			$localStorage.favorites = [];
			$localStorage.pushUrbano = true;
			$localStorage.pushAereo = true;
			$localStorage.pushHotel = true;
			$localStorage.pushLocacao = true;
			$localStorage.pushPacote = true;


			document.addEventListener("deviceready", function () {

				try{
					FCMPlugin.subscribeToTopic('urbano');

				    FCMPlugin.subscribeToTopic('aereo');

				    FCMPlugin.subscribeToTopic('hotel');

				    FCMPlugin.subscribeToTopic('locacao');

				    FCMPlugin.subscribeToTopic('pacote');
	
				}catch(e){
					alert(e);
				}

       
    		}, false);


			$localStorage.removedCacheTwoLines5 = true;
		}

		$scope.appstoreLink = 'https://play.google.com/store/apps/details?id=com.vah';

		if ($ionicPlatform.is('ios')) {
			$scope.appstoreLink = 'https://itunes.apple.com/br/app/vah-economize-tempo-e-dinheiro/id1165261052?mt=8';
		}

		$scope.showUrban = true;

		vm.fnUrban = function() {
            $window.location.href = "#/app/urban";
        }
		
		vm.fnAerial = function() {
			WebView.fnAerial();
		};

		vm.fnHotel = function() {
			WebView.fnHotel();
		};

		vm.fnRental = function() {
			WebView.fnRental();
		};	

		vm.isback = function(){

			if($localStorage.isback){
				return true;
			}else{
				return false;
			}

		}

		vm.back = function(){

			window.history.back();
			$localStorage.isback = false;

		}

		vm.goSobre = function(){


			var textAbout = "O VAH é uma startup criada por amigos de infância que resolveram se unir para fazer a melhor e mais abrangente ferramenta de comparação de preços de transporte e hospedagem do mercado. Seja para chamar um taxi da sua casa ao seu trabalho, seja para comparar as passagens aéreas das suas próximas férias ou para garantir uma promoção no hotel dos seus sonhos. Nós do VAH criamos um motor de busca (metabuscador) e exibição de resultados ultra-dinâmico fazendo com que você tenha a melhor escolha à um clique de distância. Tudo isso para que no final você consiga economizar duas coisas muito valiosas: seu tempo e seu dinheiro."

			 $ionicPopup.alert({

			 	cssClass: 'custom-class',

                template: '<div style="width: 100%; text-align: center; margin-top: 10px;"><p><img src="img/logo-inverso.png" width="100px"></p><p style="color: #004180;top: 140px;left: 50px;font-size:15px; margin-top:10px; font-weight: bold;">ECONOMIZE TEMPO E DINHEIRO</p></div><br /> <p>' + textAbout + '</p>'

            });

		}


		vm.goUse = function(){
			
			var textAbout = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

			 $ionicPopup.alert({

			 	cssClass: 'custom-class',

                template: '<div style="width: 100%; text-align: center; margin-top: 10px;"><p style="color: #004180;top: 50px;left: 50px;font-size:15px; margin-top:10px; font-weight: bold;">Termos de uso</p></div><br /> <div style="width: 100%; height: 200px; overflow-y: scroll;">' + textAbout + '</p>'

            });
			
		}


		vm.goAvalie = function(){
			
			
		}

		vm.goContact = function(){
			
			
		}


		vm.showUrban = function(){
			return $localStorage.urban == true;
		}


		vm.showAereo = function(){
			return $localStorage.aereo == true;
		}

		vm.showHotel = function(){
			return $localStorage.hotel == true;
		}

		vm.showLocacao = function(){
			return $localStorage.locacao == true;
		}

		vm.showPacotes= function(){
			return $localStorage.pacotes == true;
		}

		$scope.toggleGroup = function() {
		    if ($scope.isGroupShown()) {
		      $scope.shownGroup = false;
		    } else {
		      $scope.shownGroup = true;
		    }
		}

		$scope.isGroupShown = function() {
		    return $scope.shownGroup == true;
		  }


		$scope.isSelected = function(id){

			if(id == 1){

				return $localStorage.pushUrbano == true;

			}else if(id == 2){

				return $localStorage.pushAereo == true;

			} else if(id == 3){

				return $localStorage.pushHotel == true;

			} else if(id == 4){

				return $localStorage.pushLocacao == true;

			} else if(id == 5){

				return $localStorage.pushPacote == true;

			}

		}

		$scope.openAvalie = function(){

			 if(ionic.Platform.isIOS() || ionic.Platform.isIPad()){

                window.open('https://itunes.apple.com/br/app/vah-economize-tempo-e-dinheiro/id1165261052?mt=8','_system');

            }else if(ionic.Platform.isAndroid()){

                window.open('https://play.google.com/store/apps/details?id=com.vah&hl=pt_BR','_system');

            }else{

                window.open('https://play.google.com/store/apps/details?id=com.vah&hl=pt_BR','_system');

            }

		}

		$scope.select = function(id){

			if(id == 1){

				if($localStorage.pushUrbano == true){
					$localStorage.pushUrbano = false;

					if(window.cordova){

						FCMPlugin.unsubscribeFromTopic('urbano');

					}

				}else{
					$localStorage.pushUrbano = true;

					if(window.cordova){

						FCMPlugin.subscribeToTopic('urbano');

					}
				}

			}else if(id == 2){

				if($localStorage.pushAereo == true){
					$localStorage.pushAereo = false;

					if(window.cordova){

						FCMPlugin.unsubscribeFromTopic('aereo');

					}

			    
				}else{
					$localStorage.pushAereo = true;

					if(window.cordova){

						FCMPlugin.subscribeToTopic('aereo');

					}
				}

			} else if(id == 3){

				if($localStorage.pushHotel == true){
					$localStorage.pushHotel = false;

					if(window.cordova){

						FCMPlugin.unsubscribeFromTopic('hotel');

					}

			   
				}else{
					$localStorage.pushHotel = true;

					if(window.cordova){

						FCMPlugin.subscribeToTopic('hotel');

					}
				}

			} else if(id == 4){

				if($localStorage.pushLocacao == true){
					$localStorage.pushLocacao = false;

					 if(window.cordova){

						FCMPlugin.unsubscribeFromTopic('locacao');

					}


				}else{
					$localStorage.pushLocacao = true;

					if(window.cordova){

						FCMPlugin.subscribeToTopic('hotel');

					}
				}

			} else if(id == 5){

				if($localStorage.pushPacote == true){

					$localStorage.pushPacote = false;

			    	if(window.cordova){

						FCMPlugin.unsubscribeFromTopic('pacote');

					}

				}else{
					$localStorage.pushPacote = true;

					if(window.cordova){

						FCMPlugin.subscribeToTopic('pacote');

					}
				}

			}

		}
	});
