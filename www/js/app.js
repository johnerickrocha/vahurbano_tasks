var db = null;

angular.module('starter', ['ionic', 'ngStorage', 'ngCordova', 'ionic.contrib.NativeDrawer'])

.run(function($ionicPlatform, $localStorage, $ionicLoading, $http, $log, $ionicPopup) {
  $ionicPlatform.ready(function() {

    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }

    $ionicLoading.hide();

    if(window.cordova){

      FCMPlugin.getToken(function(token){
          console.log(token);
      });

      FCMPlugin.onTokenRefresh(function(token){
          console.log( token );
      });

      FCMPlugin.subscribeToTopic('allusers', function(message){
        console.log(message);
      }, function(err){
        console.log(err.message);
      });

      FCMPlugin.onNotification(function(data){

          if (data.link && data.link.length > 0) {

            var confirmPopup =  $ionicPopup.confirm({

              title: 'Mensagem',

              template: data.message

            });

            confirmPopup.then(function(res) {

              window.open(data.link,'_system');

            });

          }else{

            var confirmPopup =  $ionicPopup.alert({

              title: 'Mensagem',

              template: data.message

            });

            confirmPopup.then(function(res) {

              $window.location.href = '#/app/urban';

            });

          }

      });

   }

  })
})
.config(function($stateProvider, $urlRouterProvider, $httpProvider, $ionicConfigProvider) {

  $ionicConfigProvider.views.maxCache(0);

  $httpProvider.defaults.useXDomain = true;

  $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
  
  delete $httpProvider.defaults.headers.common['X-Requested-With'];
  
    $stateProvider
      .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'template.html',
        controller: 'AppCtrl',
        controllerAs: 'appCtrl'
      })
      .state('app.home', {
        url: '/home',
        views: {
          'menuContent': {
            templateUrl: 'app/home/home.view.html',
            controller: 'HomeCtrl',
            controllerAs: 'homeCtrl'
          }
        }
      })     
      .state('app.urban', {
        url: '/urban',
        views: {
          'menuContent': {
            templateUrl: 'app/urban/urban.view.html',
            controller: 'UrbanCtrl',
            controllerAs: 'urbanCtrl'
          }
        }
      })
      .state('app.admin', {
        url: '/admin',
        views: {
          'menuContent': {
            templateUrl: 'app/admin/admin.view.html',
            controller: 'AdminCtrl',
            controllerAs: 'adminCtrl'
          }
        }
      });

      $urlRouterProvider.otherwise('/app/home');

  });

